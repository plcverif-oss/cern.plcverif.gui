/*******************************************************************************
 * Copyright (c) 2003, 2018 IBM Corporation and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Lars Vogel <Lars.Vogel@gmail.com> - Bug 430694
 *     Christian Georgi (SAP)            - Bug 432480
 *     Patrik Suzzi <psuzzi@gmail.com>   - Bug 490700, 502050
 *     Vasili Gulevich                   - Bug 501404
 *     Daniel Darvas - Adapted to the needs of PLCverif. Based on org.eclipse.ui.internal.ide.application.IDEWorkbenchAdvisor
 *******************************************************************************/

package cern.plcverif.gui.eclipseutils;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.internal.resources.Workspace;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.internal.ide.IDEWorkbenchMessages;
import org.eclipse.ui.internal.ide.IDEWorkbenchPlugin;
import org.eclipse.ui.internal.progress.ProgressMonitorJobsDialog;

@SuppressWarnings("restriction")
public final class IDEWorkbenchAdvisorUtils {
	private IDEWorkbenchAdvisorUtils() {
		// Used as utility class.
	}

	public static void postShutdownWorkspaceSave() {
		IWorkspace workspace = IDEWorkbenchPlugin.getPluginWorkspace();

		final Runnable disconnectFromWorkspace = new Runnable() {
			int attempts;

			@Override
			public void run() {
				if (isWorkspaceLocked(workspace)) {
					if (attempts < 3) {
						attempts++;
						IDEWorkbenchPlugin.log(null, createErrorStatus("Workspace is locked, waiting..."));
						Display.getCurrent().timerExec(1000 * attempts, this);
					} else {
						IDEWorkbenchPlugin.log(null, createErrorStatus("Workspace is locked and can't be saved."));
					}
					return;
				}
				disconnectFromWorkspace();
			}

			IStatus createErrorStatus(String exceptionMessage) {
				return new Status(IStatus.ERROR, IDEWorkbenchPlugin.IDE_WORKBENCH, 1,
						IDEWorkbenchMessages.ProblemsSavingWorkspace, new IllegalStateException(exceptionMessage));
			}
		};

		if (workspace != null) {
			if (isWorkspaceLocked(workspace)) {
				Display.getCurrent().asyncExec(disconnectFromWorkspace);
			} else {
				disconnectFromWorkspace.run();
			}
		}
	}

	private static class CancelableProgressMonitorJobsDialog extends ProgressMonitorJobsDialog {
		public CancelableProgressMonitorJobsDialog(Shell parent) {
			super(parent);
		}

		@Override
		protected void createButtonsForButtonBar(Composite parent) {
			super.createButtonsForButtonBar(parent);
			registerCancelButtonListener();
		}

		public void registerCancelButtonListener() {
			cancel.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					subTaskLabel.setText("");
				}
			});
		}
	}

	private static boolean isWorkspaceLocked(IWorkspace workspace) {
		ISchedulingRule currentRule = Job.getJobManager().currentRule();
		return currentRule != null && currentRule.isConflicting(workspace.getRoot());
	}

	/**
	 * Disconnect from the core workspace.
	 *
	 * Locks workspace in a background thread, should not be called while
	 * holding any workspace locks.
	 */
	private static void disconnectFromWorkspace() {
		// save the workspace
		final MultiStatus status = new MultiStatus(IDEWorkbenchPlugin.IDE_WORKBENCH, 1,
				IDEWorkbenchMessages.ProblemSavingWorkbench, null);
		try {
			final boolean applyPolicy = ResourcesPlugin.getWorkspace().getDescription().isApplyFileStatePolicy();
			final ProgressMonitorJobsDialog p = new CancelableProgressMonitorJobsDialog(null);
			IRunnableWithProgress runnable = monitor -> {
				try {
					if (applyPolicy) {
						status.merge(((Workspace) ResourcesPlugin.getWorkspace()).save(true, true, monitor));
					}
				} catch (CoreException e) {
					status.merge(e.getStatus());
				}
			};

			p.run(true, false, runnable);
		} catch (InvocationTargetException e) {
			status.merge(new Status(IStatus.ERROR, IDEWorkbenchPlugin.IDE_WORKBENCH, 1,
					IDEWorkbenchMessages.InternalError, e.getTargetException()));
		} catch (InterruptedException e) {
			status.merge(new Status(IStatus.ERROR, IDEWorkbenchPlugin.IDE_WORKBENCH, 1,
					IDEWorkbenchMessages.InternalError, e));
		}
		ErrorDialog.openError(null, IDEWorkbenchMessages.ProblemsSavingWorkspace, null, status,
				IStatus.ERROR | IStatus.WARNING);
		if (!status.isOK()) {
			IDEWorkbenchPlugin.log(IDEWorkbenchMessages.ProblemsSavingWorkspace, status);
		}
	}
}
