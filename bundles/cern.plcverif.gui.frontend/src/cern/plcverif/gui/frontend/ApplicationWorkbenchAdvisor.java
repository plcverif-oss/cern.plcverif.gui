/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.gui.frontend;

import java.net.URL;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IWorkbenchPreferenceConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.ide.IDEInternalWorkbenchImages;
import org.eclipse.ui.internal.ide.IDEWorkbenchPlugin;
import org.osgi.framework.Bundle;

import cern.plcverif.gui.eclipseutils.IDEWorkbenchAdvisorUtils;

@SuppressWarnings("restriction")
public class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

	private static final String PERSPECTIVE_ID = "cern.plcverif.gui.frontend.perspective"; //$NON-NLS-1$
	private static final String ICONS_PATH = "icons/full/";

	@Override
	public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		return new ApplicationWorkbenchWindowAdvisor(configurer);
	}

	@Override
	public String getInitialWindowPerspectiveId() {
		return PERSPECTIVE_ID;
	}

	@Override
	public String getMainPreferencePageId() {
		return "cern.plcverif.base.gui.preferences.rootpage";
	}

	@Override
	public void initialize(IWorkbenchConfigurer configurer) {
		super.initialize(configurer);

		configurer.setSaveAndRestore(true);
		PlatformUI.getPreferenceStore().setValue(IWorkbenchPreferenceConstants.CLOSE_EDITORS_ON_EXIT, true);
		PlatformUI.getPreferenceStore().setValue(IWorkbenchPreferenceConstants.NO_NEW_PERSPECTIVE, false);
		PlatformUI.getPreferenceStore().setValue(IWorkbenchPreferenceConstants.SHOW_OPEN_ON_PERSPECTIVE_BAR, true);
		PlatformUI.getPreferenceStore().setValue(IWorkbenchPreferenceConstants.SHOW_OTHER_IN_PERSPECTIVE_MENU, false);
		PlatformUI.getPreferenceStore().setValue(IWorkbenchPreferenceConstants.SHOW_MEMORY_MONITOR, true);
		PlatformUI.getPreferenceStore().setValue(IWorkbenchPreferenceConstants.SHOW_SYSTEM_JOBS, false);

		// To restore the icons in Project View
		// source:
		// http://stackoverflow.com/questions/8277902/add-package-explorer-in-rcp-application-result-in-loosing-some-icon
		// and
		// https://www.eclipse.org/forums/index.php/t/1088678/
		// (images are in org.eclipse.ui.ide)
		IDE.registerAdapters();

		final Bundle ideBundle = Platform.getBundle(IDEWorkbenchPlugin.IDE_WORKBENCH);
		declareWorkbenchImage(configurer, ideBundle, IDE.SharedImages.IMG_OBJ_PROJECT, ICONS_PATH + "obj16/prj_obj.png",
				true);
		declareWorkbenchImage(configurer, ideBundle, IDE.SharedImages.IMG_OBJ_PROJECT_CLOSED,
				ICONS_PATH + "obj16/cprj_obj.png", true);
		declareWorkbenchImage(configurer, ideBundle, IDEInternalWorkbenchImages.IMG_ETOOL_PROBLEMS_VIEW,
				ICONS_PATH + "eview16/problems_view.png", true);
		declareWorkbenchImage(configurer, ideBundle, IDEInternalWorkbenchImages.IMG_ETOOL_PROBLEMS_VIEW_ERROR,
				ICONS_PATH + "eview16/problems_view_error.png", true);
		declareWorkbenchImage(configurer, ideBundle, IDEInternalWorkbenchImages.IMG_ETOOL_PROBLEMS_VIEW_WARNING,
				ICONS_PATH + "eview16/problems_view_warning.png", true);
		declareWorkbenchImage(configurer, ideBundle, IDEInternalWorkbenchImages.IMG_ETOOL_PROBLEMS_VIEW_INFO,
				ICONS_PATH + "eview16/problems_view_info.png", true);
		declareWorkbenchImage(configurer, ideBundle, IDEInternalWorkbenchImages.IMG_OBJS_ERROR_PATH,
				ICONS_PATH + "obj16/error_tsk.png", true);
		declareWorkbenchImage(configurer, ideBundle, IDEInternalWorkbenchImages.IMG_OBJS_WARNING_PATH,
				ICONS_PATH + "obj16/warn_tsk.png", true);
		declareWorkbenchImage(configurer, ideBundle, IDEInternalWorkbenchImages.IMG_OBJS_INFO_PATH,
				ICONS_PATH + "obj16/info_tsk.png", true);
	}

	private static void declareWorkbenchImage(final IWorkbenchConfigurer workbenchConfig, final Bundle ideBundle,
			final String symbolicName, final String path, final boolean shared) {
		final URL url = ideBundle.getEntry(path);
		final ImageDescriptor desc = ImageDescriptor.createFromURL(url);
		workbenchConfig.declareImage(symbolicName, desc, shared);
	}

	@Override
	public void postShutdown() {
		IDEWorkbenchAdvisorUtils.postShutdownWorkspaceSave();
		super.postShutdown();
	}
}
