/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.gui.frontend.perspectives;

import java.io.PrintStream;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class PlcverifPerspective implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		// Redirect console
		if (!"true".equalsIgnoreCase(System.getProperty("plcverif.noConsoleRedirection"))) {
			// Check if -Dplcverif.noConsoleRedirection=true is present
			redirectConsole();
		}

		// Define layout
		layout.setEditorAreaVisible(true);
		String editorArea = layout.getEditorArea();
		
		// Left side
		String leftId = "cern.plcverif.layout.left";
		layout.createFolder(leftId, IPageLayout.LEFT, 0.2f, editorArea);
		
		IFolderLayout topLeft = layout.createFolder("topLeft", IPageLayout.TOP, 0.6f, leftId);
		topLeft.addView(IPageLayout.ID_PROJECT_EXPLORER);
		IFolderLayout bottomLeft = layout.createFolder("bottomLeft", IPageLayout.BOTTOM, 0.4f, leftId);
		bottomLeft.addView(IPageLayout.ID_OUTLINE);
		
		// Bottom side
		IFolderLayout bottom = layout.createFolder("bottom", IPageLayout.BOTTOM, 0.7f, editorArea);
		bottom.addView(IPageLayout.ID_PROGRESS_VIEW);
		bottom.addView(IPageLayout.ID_PROBLEM_VIEW);
		bottom.addView(IConsoleConstants.ID_CONSOLE_VIEW);

		// set views non-closeable
		layout.getViewLayout(IPageLayout.ID_PROJECT_EXPLORER).setCloseable(false);
		layout.getViewLayout(IPageLayout.ID_PROGRESS_VIEW).setCloseable(false);
	}

	private void redirectConsole() {
		// Based on
		// http://jprog.blogspot.com/2005/09/using-eclipse-console-view-in-rcp.html

		ImageDescriptor icon = ImageDescriptor.createFromURL(getClass().getResource("/icons/plcverif_icon1_16x16.png"));
		MessageConsole plcverifConsole = new MessageConsole("PLCverif console", icon);
		ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[] { plcverifConsole });

		MessageConsoleStream stream = plcverifConsole.newMessageStream();
		PrintStream plcverifStream = new PrintStream(stream);
		System.setOut(plcverifStream); // link standard output stream to the console
		System.setErr(plcverifStream); // link error output stream to the console
	}
}
