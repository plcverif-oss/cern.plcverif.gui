/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.gui.frontend;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.preference.IPreferenceNode;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.navigator.resources.ProjectExplorer;

public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

	public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		super(configurer);
	}

	@Override
	public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
		return new ApplicationActionBarAdvisor(configurer);
	}

	@Override
	public void preWindowOpen() {
		IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
		configurer.setInitialSize(new Point(1000, 700));
		configurer.setShowCoolBar(true);
		configurer.setShowStatusLine(true);
		configurer.setShowMenuBar(false);
		configurer.setShowPerspectiveBar(true);
		configurer.setShowProgressIndicator(true);
	}

	@Override
	public void postWindowOpen() {
		super.postWindowOpen();

		// start in maximized window
		getWindowConfigurer().getWindow().getShell().setMaximized(true);

		// Workaround to show the contents of Package Explorer on opening
		// source: http://www.eclipse.org/forums/index.php?t=msg&goto=486705&
		IWorkbenchWindow[] workbenches = PlatformUI.getWorkbench().getWorkbenchWindows();

		for (IWorkbenchWindow workbench : workbenches) {
			for (IWorkbenchPage page : workbench.getPages()) {
				IViewPart view = page.findView("org.eclipse.ui.navigator.ProjectExplorer");
				if (view != null && view instanceof ProjectExplorer) {
					((ProjectExplorer) view).getCommonViewer().setInput(ResourcesPlugin.getWorkspace().getRoot());
					continue;
				}
			}
		}

		// Workaround to hide the Search menu
		// from http://www.eclipse.org/forums/index.php/t/160714/
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
				.hideActionSet("org.eclipse.search.searchActionSet");

		// Clean the Preferences window
		removeJdtPreferencePages();
	}

	@Override
	public boolean preWindowShellClose() {
		// Try to close web browser views (to avoid automated reopening, as they
		// will not open with the right URL)
		for (IWorkbenchWindow workbench : PlatformUI.getWorkbench().getWorkbenchWindows()) {
			for (IWorkbenchPage page : workbench.getPages()) {
				// Normal findView would not work due to the potential secondary ID that is unknown here
				for (IViewReference viewRef : page.getViewReferences()) {
					if (viewRef.getId().equals("org.eclipse.ui.browser.view")) {
						page.hideView(viewRef);
					}
				}
			}
		}

		return super.preWindowShellClose();
	}

	protected static final Set<String> JDT_PREF_PAGE_IDS = new HashSet<>(Arrays.asList(
			// Java page
			"org.eclipse.jdt.ui.preferences.JavaBasePreferencePage",
			// Run/Debug page
			"org.eclipse.debug.ui.DebugPreferencePage"));

	private static void removeJdtPreferencePages() {
		// Based on:
		// http://rcpexperiments.blogspot.fr/2010/03/how-to-remove-unwanted-preference-pages.html
		PreferenceManager preferenceManager = PlatformUI.getWorkbench().getPreferenceManager();
		IPreferenceNode[] rootPrefPages = preferenceManager.getRootSubNodes();

		for (IPreferenceNode preferenceNode : rootPrefPages.clone()) {
			if (JDT_PREF_PAGE_IDS.contains(preferenceNode.getId())) {
				preferenceManager.remove(preferenceNode);
			}
		}
	}
}
