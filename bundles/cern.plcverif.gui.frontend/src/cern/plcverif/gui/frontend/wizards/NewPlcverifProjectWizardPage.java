/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.gui.frontend.wizards;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;

import com.google.common.base.Preconditions;

public class NewPlcverifProjectWizardPage extends WizardNewProjectCreationPage {
	private static final String BUILTIN_PROJECT_PREFIX = ".builtin_";
	private BuiltinProjectDependenciesArea dependenciesArea;

	public NewPlcverifProjectWizardPage() {
		super("NewPlcverifProjectWizardPage");
		this.setTitle("PLCverif project");
		this.setDescription(
				"This wizard creates a new verification project. A verification project can contain multiple source files and verification cases.");
	}

	@Override
	public void createControl(Composite parent) {
		super.createControl(parent);
		Preconditions.checkState(getControl() instanceof Composite);
		Composite control = (Composite) getControl();

		dependenciesArea = new BuiltinProjectDependenciesArea();
		dependenciesArea.createContents(control);

		parent.requestLayout();
	}

	/**
	 * Returns the project that needs to be added as a project dependency to
	 * handle the built-in blocks and functions.
	 * 
	 * @return Project to be added as dependency, or empty.
	 */
	public Optional<IProject> getBuiltinDependenciesProject() {
		return dependenciesArea.getSelectedProject();
	}

	public static class BuiltinProjectDependenciesArea {
		private IProject currentlySelectedProject = null;
		private boolean enabled = true;
		private Map<String, IProject> knownProjects = null;

		// GUI
		private Button checkbox;
		private Combo combo;
		private Label comboLabel;

		private void createContents(Composite parent) {
			Composite composite = new Composite(parent, SWT.NONE);
			GridDataFactory.fillDefaults().grab(true, false).applyTo(composite);
			GridLayoutFactory.swtDefaults().numColumns(2).applyTo(composite);

			checkbox = new Button(composite, SWT.CHECK | SWT.LEFT);
			checkbox.setText("Use built-in functions");
			GridDataFactory.fillDefaults().span(2, SWT.DEFAULT).applyTo(checkbox);
			checkbox.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					super.widgetSelected(e);
					setEnabled(checkbox.getSelection());

				}
			});

			comboLabel = new Label(composite, SWT.NONE);
			comboLabel.setText("Built-in block project:");
			GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).applyTo(comboLabel);

			combo = new Combo(composite, SWT.READ_ONLY);
			GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.CENTER).hint(200, SWT.DEFAULT)
					.applyTo(combo);
			combo.addModifyListener(new ModifyListener() {
				@Override
				public void modifyText(ModifyEvent e) {
					if (knownProjects != null && knownProjects.containsKey(combo.getText())) {
						currentlySelectedProject = knownProjects.get(combo.getText());
					} else {
						currentlySelectedProject = null;
					}

				}
			});
			fillCombobox();

			setEnabled(false);
		}

		private void setEnabled(boolean enabled) {
			comboLabel.setEnabled(enabled);
			combo.setEnabled(enabled);

			this.enabled = enabled;
		}

		private void fillCombobox() {
			this.knownProjects = new HashMap<String, IProject>();

			for (IProject p : ResourcesPlugin.getWorkspace().getRoot().getProjects()) {
				if (p.getName().startsWith(BUILTIN_PROJECT_PREFIX)) {
					String projectDisplayName = p.getName().substring(BUILTIN_PROJECT_PREFIX.length());
					combo.add(projectDisplayName);

					this.knownProjects.put(projectDisplayName, p);
				}
			}

			// Select the first one
			if (combo.getSelectionIndex() < 0 && combo.getItemCount() > 0) {
				combo.select(0);
			}
		}

		public Optional<IProject> getSelectedProject() {
			if (enabled) {
				return Optional.ofNullable(this.currentlySelectedProject);
			} else {
				return Optional.empty();
			}
		}
	}
}
