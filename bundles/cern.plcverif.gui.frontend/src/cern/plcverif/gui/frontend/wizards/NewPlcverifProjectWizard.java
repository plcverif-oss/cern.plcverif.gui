/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.gui.frontend.wizards;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.internal.progress.ProgressManagerUtil;
import org.eclipse.xtext.ui.XtextProjectHelper;

@SuppressWarnings("restriction")
public class NewPlcverifProjectWizard extends Wizard implements INewWizard {
	private NewPlcverifProjectWizardPage wizardPage;

	public static final String ID = "cern.plcverif.gui.frontend.wizards.NewPlcverifProjectWizard";

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.setWindowTitle("New PLCverif project");
		this.wizardPage = new NewPlcverifProjectWizardPage();
	}

	@Override
	public boolean performFinish() {
		try {
			WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
				@Override
				protected void execute(final IProgressMonitor monitor) {
					NewPlcverifProjectWizard.this.createProject(monitor != null ? monitor : new NullProgressMonitor());
				}
			};
			this.getContainer().run(false, true, op);
		} catch (InvocationTargetException | InterruptedException e) {
			MessageDialog.openError(ProgressManagerUtil.getDefaultParent(), "Error", e.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public void addPages() {
		this.addPage(this.wizardPage);
	}

	protected final void createProject(final IProgressMonitor monitor) {
		monitor.beginTask("Creating project", 1);
		try {
			// Create a new project.
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			IProject project = root.getProject(this.wizardPage.getProjectName());

			// Set the project's location.
			IProjectDescription description = ResourcesPlugin.getWorkspace().newProjectDescription(project.getName());
			if (!Platform.getLocation().equals(this.wizardPage.getLocationPath())) {
				description.setLocation(this.wizardPage.getLocationPath());
			}

			// Add Xtext nature to the project
			description.setNatureIds(new String[] { XtextProjectHelper.NATURE_ID });

			// Add the Xtext builder to the project
			ICommand command = description.newCommand();
			command.setBuilderName(XtextProjectHelper.BUILDER_ID);
			description.setBuildSpec(new ICommand[] { command });

			// Set eventual references
			if (wizardPage.getBuiltinDependenciesProject().isPresent()) {
				description.setReferencedProjects(new IProject[] { wizardPage.getBuiltinDependenciesProject().get() });
			}

			// Perform the actual creation and open the newly created project
			project.create(description, monitor);
			monitor.worked(1);
			project.open(monitor);
		} catch (CoreException e) {
			MessageDialog.openError(ProgressManagerUtil.getDefaultParent(), "Error", e.getMessage());
		} finally {
			monitor.done();
		}
	}

}
