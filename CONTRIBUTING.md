# Contributing to PLCverif

Have something you'd like to contribute to PLCverif? We welcome merge requests but ask that you carefully read this document first to understand how best to submit them; what kind of changes are likely to be accepted; and what to expect from the PLCverif team when evaluating your submission.

*Please refer back to this document as a checklist before issuing any pull request; this will save time for everyone!*

## Take your first steps

### Understand the basics

Not sure what a merge request is, or how to submit one? Take a look at GitLab's excellent [help documentation](https://docs.gitlab.com/ee/user/project/merge_requests/) first.

### Search issues and create one if necessary

Is there already an issue that addresses your concern? Do a bit of searching in our [issue tracker](https://gitlab.com/groups/plcverif-oss/-/issues) to see if you can find something similar. If you do not find something similar, please create a new issue before submitting a merge request. We are asking you to systematically create an issue even for trivial changes such as removing compiler warnings, comments, etc.

## Set up a development environment

Please refer to the [deveopper documentation] (https://plcverif-oss.gitlab.io/plcverif-docs/development/DevGettingStarted.html) to set up your own environment to develop, test and validate your changes.

## Create a branch

### Branch from `master`

Name the branch based on the issue number or even better let GitLab create the branch for you directly from the issue page.

Please submit all merge requests to master, even bug fixes and minor improvements. 

## PLCverif code style

### Code style

Please refer to the [PLCverif code style guide](https://plcverif-oss.gitlab.io/plcverif-docs/development/DevGuidelines.html)

### Add the EPL 2 license header to all new files

```java
/*******************************************************************************
 * (C) Copyright CERN 2017-2020. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   FirstName LastName - initial API and implementation
 *******************************************************************************/

```

### Update the license header file as necessarry

Always check the date range in the license header. For example, if you've
modified a file in 2020 whose header still reads:

```java
/*
 * Copyright (C) 2010-2015 CERN. All rights not expressly granted are reserved.
```

Then be sure to update it to 2020 accordingly:

```java
/*
 * Copyright (C) 2010-2020 CERN. All rights not expressly granted are reserved.
```

### Comments

Write comments if appropriate:
* always write documentation comments to classes and non-overriding public methods
* don't write meaningless comments, e.g. *Getting the value. @returns The Value* for a `getValue` method. Use meaningful names (variables and functions) and less comments.

### Test

Write testable code and write test. 

## Prepare your merge request

### Submit JUnit test cases for all behaviour changes

Search the codebase to find related tests and add additional @Test methods as appropriate.

### Format merge request messages

Please format your messages in the following way:

    #25: Short (50 chars or less) summary of changes

    More detailed explanatory text, if necessary. Wrap it to about 72
    characters or so. In some contexts, the first line is treated as the
    subject of an email and the rest of the text as the body. The blank
    line separating the summary from the body is critical (unless you omit
    the body entirely); tools like rebase can get confused if you run the
    two together.

    Further paragraphs come after blank lines.

     - Bullet points are okay, too

     - Typically a hyphen or asterisk is used for the bullet, preceded by a
       single space, with blank lines in between, but conventions vary here


* Prefix the subject line with the a hash symbol, followed by the related
   issue number, followed by a colon and a space, e.g. "#25: Fix ...".
* Use imperative statements in the subject line, e.g. "Fix broken Javadoc link".
* Begin the subject line with a capitalized verb, e.g. "Add, Prune, Fix,
    Introduce, Avoid" etc.
* Do not end the subject line with a period.
* Restrict the subject line to 50 characters or less if possible.
* Wrap lines in the body at 72 characters or less.
* Mention associated issue(s) at the end of the commit comment, prefixed
    with "Issue: " as above.
* In the body of the commit message, explain how things worked before this
    commit, what has changed, and how things work now.
* _Check your spelling!_

## Run the Final Checklist

### Run all tests prior to submission

Make sure that all tests pass prior to submitting your merge request. Run the command `mvn clean verify` on the project you modified. Do not submit the merge request until the build is successful.

### Expect discussion and rework

The PLCverif team takes a very conservative approach to accepting contributions to
the framework. This is to keep code quality and stability as high as possible,
and to keep complexity at a minimum. Your changes, if accepted, may be heavily
modified prior to merging. You will retain "Author:" attribution for your Git
commits granted that the bulk of your changes remain intact. You may be asked to
rework the submission for style (as explained above) and/or substance. Again, we
strongly recommend discussing any serious submissions with the PLCverif
team _prior_ to engaging in serious development work.
