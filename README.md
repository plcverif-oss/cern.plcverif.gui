# cern.plcverif.gui

## PLCverif GUI executables 

| Platform | Release |
| --- | --- |
| Windows | [zip](https://plcverif-oss.gitlab.io/cern.plcverif.gui/releases/cern.plcverif.gui.product-win32.win32.x86_64.zip)  |
| Linux | [tar.gz](https://plcverif-oss.gitlab.io/cern.plcverif.gui/releases/cern.plcverif.gui.product-linux.gtk.x86_64.tar.gz) |
| Mac OS X (*) | [tar.gz](https://plcverif-oss.gitlab.io/cern.plcverif.gui/releases/cern.plcverif.gui.product-macosx.cocoa.x86_64.tar.gz) |

On all platforms **Java 11** is required.

(*) Mac OS X installation notes:
- For Mac OS X, you may want to check [this page](https://secure.clcbio.com/helpspot/index.php?pg=kb.page&id=323#:~:text=If%20you%20are%20working%20on,you%20try%20to%20launch%20it.&text=You%20should%20eject%20the%20disk,move%20it%20to%20the%20Trash.%22) in case you are facing the error _PLCverif is damaged and can't be opened_

## PLCverif documentation
| Type | PDF | HTML |
| ---- | --- | ---- |
| Full developer documentation | [PDF](https://plcverif-oss.gitlab.io/plcverif-docs/devdocs.pdf) | [HTML](https://plcverif-oss.gitlab.io/plcverif-docs/) |
| User documentation | [PDF](https://plcverif-oss.gitlab.io/plcverif-docs/userdocs.pdf) | [HTML](https://plcverif-oss.gitlab.io/plcverif-docs/userdocs/) | 

## Other links specific to cern.plcverif.gui
[p2 repository](https://plcverif-oss.gitlab.io/cern.plcverif.gui/p2/p2.index)

[Javadoc](https://plcverif-oss.gitlab.io/cern.plcverif.gui/javadoc/)

[Coverage report](https://plcverif-oss.gitlab.io/cern.plcverif.gui/jacoco)


